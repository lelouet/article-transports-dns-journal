#!/bin/bash

set -e

if ! [ -r Makefile  ];
then
   echo "Working directory must contain the Makefile"
   exit 1
fi

USER_ID="$(id -u)"
GROUP_ID="$(id -g)"

echo "building docker image, uid=$USER_ID, gid=$GROUP_ID"

docker build -t lelouet/debian-latexmk \
       --build-arg USER_ID="$USER_ID" \
       --build-arg GROUP_ID="$GROUP_ID" \
       .

echo "running container"

if [ -n "$1" ];
then
   docker run -it --rm \
	  -v "$(pwd)":/src \
	  lelouet/debian-latexmk "$1"
else
    docker run -it --rm \
	  -v "$(pwd)":/src \
	  lelouet/debian-latexmk
fi

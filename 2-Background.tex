\begin{figure}[ht]
  \centering
    \includesvg[inkscapelatex=false, width = 0.9\columnwidth]{dns_msg_exchange}
	\caption{Comparison of DNS over UDP, TCP, TLS (DoT) and HTTP/2 (DoH)}
	\label{fig:messages}
\end{figure}

From a high-level point of view, DNS is a registry service queried by a client to resolve the IP address corresponding to a domain name. It has been implemented as a tree-like database, in which multiple servers hold only a fraction of the information~: 
%
looking for information on it is therefore a depth-first search starting from the root.
%
However, if every client looking to translate a domain name to an IP address were to realise such a process, it would result in prohibitive latency and an overload on the servers closest to the root;
%
that is why iterating through the tree is delegated to resolvers, servers which, upon receiving a query from a client, can answer it either from their cache, that will have a higher \textit{hit rate} since it likely received the same request from another client earlier, or by doing the resolution themselves. \ITUpar

In contrast to the other, historically text-based, web protocols, the DNS message protocol has been implemented using binary message format in order to target the highest performances. Both UDP and TCP have been selected as transports.
%
The first one, UDP, is connection-less (Figure \ref{fig:messages}~a), and provides high performances at the cost of reliability and message payload size. As such, it is the recommended protocol to transport standard DNS queries (which represents most of the DNS traffic). The second one, TCP, requires the exchange of three messages (arrows 1 through 3 on figures \ref{fig:messages}~b~c~d) to establish a connection, before sending any DNS messages. It has been mostly used to transport special DNS messages (zone transfers) that do not fit into an UDP datagram.
%
As these legacy DNS transport protocols are unsecured, DNS is vulnerable to a variety of attacks detailed in section \ref{sec:related_works}. Several secured protocols have been proposed to deal with the aforementioned security flaws. \ITUpar

The first one, DoT \cite{rfc7858}, uses a TLS connection \cite{rfc8446} to provide both integrity and confidentiality. It relies on a TCP connection to establish a TLS session between the client and the server. Two messages (arrows 3 and 4 in figure \ref{fig:messages} c) are exchanged between both endpoints to derive, from their respective pairs of asymmetric keys, a symmetric key used to encrypt the DNS binary messages. During this process, the client also validates the identity of the resolver by using the latter’s digital certificate. \ITUpar

DoH has been proposed as an alternative to offer a secure DNS transport. It relies on HTTP/2 \cite{rfc7540} to carry the DNS messages, and may be seen as an additional layer built on top of TLS. Once a session is established (arrows 3 through 4 in fig~\ref{fig:messages}), the multiple streams of the HTTP/2 protocol are used to transport DNS queries, either directly in their binary format as the body of an HTTP \gls{POST} query (arrows 5, 6, 7 and 8 in figure \ref{fig:messages}~d) or as the base64-encoded \acrfull{url} parameter of a \gls{GET} query (as it is less common it is not considered here). \ITUpar

Originally designed for performance, the legacy DNS protocol doesn’t offer any security guarantees. To prevent data leaks and corruption, new protocols based on existing technologies used to guarantee security on the web have been pushed in order to secure DNS. Switching from a connection-less un-encrypted protocol to connected ones making extensive use of cryptography seems to go against the original goals that drove to the development of DNS, \replaceforme{therefore,}{and therefore} the cost of this transition must be analysed.

Moving from UDP, an unconnected protocol historically used for communication between clients and resolvers, to more complex connected ones can lead to an increase in consumption of hardware resources on the resolver side~:
%
the handling, by the resolver, of DNS queries transported in a UDP datagram simply requires receiving the datagram and then sending another one containing the answer once the cache-lookup or resolution is completed.
%
On the other hand, the use of session-based protocols is more complex, as they require multiple round trips and additional \replaceforme{computing}{computations} for \replaceforme{establishment}{the establishment} of a session, \replaceforme{and then}{} the management of the state associated with said session, \replaceforme{as well as}{} the encoding and decoding of messages \replaceforme{in order to receive queries and emit responses}{}, in addition with the already-existing cost of handling DNS queries. \ITUpar

Questions about scalability and resource consumption arise regarding the cost of these additional steps. In order to properly evaluate their cost, we realised a series of synthetic benchmarks, first using DNS over UDP (UDP) as a baseline, then DNS over TCP (TCP), DNS over TLS (DoT), and DNS over HTTPS (DoH).
%
While it offers no privacy guarantees, measuring how TCP performs is still interesting because, as we have seen previously in section \ref{sec:technical_background}, both secured protocols have been built on top of TCP. Thus, the comparison between UDP and TCP is a good performance indicator of the cost added by the TCP connection.
%
Using the same approach, comparing TCP and DoT \replaceforme{allow}{allows} us to measure the performance cost of the TLS session establishment and traffic encryption, and comparing DoT with DoH gives us insights about the cost of the added HTTP/2 layers. \ITUpar

In section~\ref{subsection:setup_expe}, we describe the experimental environment of our benchmarks. Section~\ref{subsection:udp} presents the results of a benchmark of the legacy UDP-based protocol, while Sections~\ref{subsection:upkeep}, \ref{subsection:use} and \ref{subsection:establishment} describe the multiple synthetic benchmarks we realised to characterise the costs of the different steps of the connection-based protocols.

\subsection{Experimental setup}
\label{subsection:setup_expe}

We elected to run our benchmarks on a DNS architecture deployed on the Grid5000 \cite{grid5000} platform. % Figure \ref{fig:experimental_setup} presents a recapitulative view of our experimental setup.
%
Our test bed is composed of 22 Dell PowerEdge R640, each of them having an 18-core CPU with a base clock of 2.2 GHz and a turbo frequency of 3.9 GHz, 96 GiB of RAM (Random Access Memory) and a 25 Gbps \acrfull{nic}, all connected together through the same switch.
%
We run our server on one of those machines, use twenty of them as our clients and the remaining one as experiment monitor in charge of deploying and running the various actors and measurement tools on their respective machines. \ITUpar

We selected three secured DNS implementations to test: Knot-resolver \cite{knot-resolver}, as it is used by important industry players (most notably Cloudflare), Dnsdist \cite{dnsdist}, that is not a server \textit{per se}, but acts as a proxy and load balancer between a client and another server, it can either answer from its cache, or forward the query to another server. Since it is compatible with both DoH and DoT, it can be used to modernise an existing DNS infrastructure by adding support for these protocols without having to make extensive modifications to the underlying server(s) software or configuration. The last implementation we tested is Adguard-Dnsproxy \cite{adguard-dnsproxy}, as it is a relatively recent implementation that promises support for newer protocols such as DNS over QUIC or DNS over HTTP/3.
%
As we need a very high number of clients to reach 100\% load on one core in our setup, we configure both software to only run on a single core of our server machine using Linux \textit{cgroups}. \ITUpar

As we aim to focus on the server-side cost of transitioning from a legacy UDP based protocol to a session-based protocols for the client to server connections,
%
we decided to exclude the cost of retrieving the records from the hierarchy of DNS name servers from the resolving process, to avoid measurements noise that could occur when querying external uncontrolled name servers.
%
At the beginning of each experiment, we fill the cache of our servers with the DNS records that will be queried during the experiment, meaning that all subsequent clients queries resulting in a cache hit. To reduce experimental variability as much as possible, all of the names that are queried for during the rest of the experiment are composed of a number from 0 to 2000, padded to four characters, followed by a non-existing top-level domain. \ITUpar

Traffic is generated using Flamethrower \cite{flamethrower}, a DNS benchmarking utility compatible with all benchmarked protocols.
%
We patched its code so it would be able to keep the underlying connections opened for a configurable duration, as its default behaviour was to open a connection, send a set number of queries, wait for the answers to these queries (up to a configurable timeout), and then close the connection, only to re-open it again for the next batch of queries. As we wanted to be able to control the length of the connections, our new implementation opens a connection for a set duration, sends batches of queries on that connection at a configurable frequency (waiting for their answer or timeout), and only closes the connection once a configurable timer, separate timer has run out. A fork of Flamethrower including these changes is available on GitHub \cite{flamethrower-lelouet}.
%
When benchmarking DoH, we send our queries in the body of an \replaceforme{POST HTTP/2}{HTTP/2 POST} query, as we detected in section \ref{sec:characterization_DNS_client_behaviour} that it was how clients operated.
%
\replacefond{}{For all these experiments, we tuned Flamethrower's parameters (number of queries per batch, and delay between each batch), so that our resolver process would end up using 100\% of the CPU it was pinned on (as measured by both htop, ps, and confirmed by the output of linux-perf).}


\subsection{Baseline}
\label{subsection:udp}

As it is the legacy, most widely used and the most efficient transport for DNS, measuring how UDP performs gives us a baseline in terms of performances.
%
\replacefond{Therefore, we benchmark our servers by sending as many requests as possible, stopping only when we started recording systematic losses.}{
To obtain this baseline, we run several experiments in which DNS over UDP queries at a set rate, which increase each experiment.
As UDP offers no flow control mechanism, at some point the client will start sending more traffic than the server can answer, meaning
that it will not answer some of them, leading to losses. We then take note of the maximum amount of queries per second handled by the server, and use it
as our baseline.}

At best, Knot-resolver answered 105~000 QPS out of the 120~000 QPS sent by our clients, while Dnsdist answered 223~000 QPS, out of the 240~000 sent.
%
We investigated these losses and noticed that they were due to a saturation of the CPU, both servers being unable to process queries at such a rate, leading to the kernel-side UDP reception buffer filling up and packets having to be discarded. Increasing the size of the reception buffer is useless~: since we send queries at a constant rate, it will just delay the moment said buffer fills up and the kernel starts discarding packets.
%
We explain the difference in performances of almost 50\% between Knot-resolver and Dnsdist by the fact that Dnsdist is a proxy and load balancer whose purpose is to pass queries to an upstream server as efficiently as possible, therefore having very few things to do when receiving a query other than answering it from its cache or forwarding it, while Knot-resolver most likely has to do more processing, even in the case of a cache hit (query policy, response padding).

\begin{figure}[!ht]
\centering
\subfloat[Knot-resolver]{%
  \includesvg[inkscapelatex=false,width=\columnwidth]{memory_knot_halfwidth}
}

\subfloat[Dnsdist]{%
  \includesvg[inkscapelatex=false,width=\columnwidth]{memory_dnsdist_halfwidth}
}

\subfloat[Adguard-dnsproxy]{%
  \includesvg[inkscapelatex=false,width=\columnwidth]{memory_adguard_halfwidth}
}
\caption{Memory usage of the servers relative to the number of connections}
\label{fig:conn_memory}
\end{figure}

\subsection{Memory usage of keeping connections alive}
\label{subsection:upkeep}

Transitioning from UDP, a connection-less protocol, to connection-based ones raises the question of the maximum number of simultaneous connections a server can handle. Therefore, we have devised an experiment aiming to measure the limits (in terms of memory) of the number of connections that can be handled by a server. \ITUpar 

For each protocol we tested (TCP, DoT and DoH), we used Flamethrower to generate as many connections towards our server as possible, spread across our 20 machines.
%
We configured both client and server so that they would not close established connections, and increased the kernel-side limits on the number of outgoing or incoming connections.
%
We measure two separate values: the Resident Set Size (amount of memory used by a process present in physical RAM) of the server, and the total amount of memory used on the machine.
%
By calculating the difference between these two values, we are able to estimate the amount of memory used by the kernel, as the in-physical RAM memory imprint of the processes other than the server is negligible.
%
There was an option in Dnsdist allowing for the release of memory linked idle connections, which we chose to deactivate as our interest lied in estimating how much memory an active connection would consume. \ITUpar 

Figure \ref{fig:conn_memory} shows, for each server and protocol combination, the total physical memory used relative to the number of connections. At the top (in red) is the resident set size of the server, in the middle (in black) is memory used by the kernel, and, at the bottom (in grey) is the amount of memory used when the server is idle, presented for reference.
%
The memory used by the kernel is the same in every experiment, which is consistent with the fact that the kernel only handles TCP connections, the common part between the three protocols.
%
For all servers, we observe an increase in memory consumption when switching from TCP to DoT, as the handling of TLS sessions requires additional state, handled by the server.
%
When considering the difference between DoT and DoH, we can notice, for both Adguard-dnsproxy and Knot-resolver, a clear increase in consumption induced by the use of DoH, due to the fact that their DoH stack is built upon their TLS stack. Therefore the memory consumed by HTTP2's protocol layers are added to the memory consumed by the TLS layer.
%
For Dnsdist however, we observe that DoH has a lower memory consumption than DoT. While this seems counter-intuitive, it is consistent with the memory consumption per connection and protocol announced in its documentation (that advertises a higher per-connection memory consumption for DoT than for DoH).
%
As the number of simultaneous connections never reaches 400~000 in the following experiments, we conclude that memory consumption won't be the limiting factor in our setup.

\begin{table}[!ht]
\begin{adjustbox}{max width=\columnwidth}
\begin{tabular}{|c|c|c|}
\hline
Software                           & Protocol & Number of connections                                            \\ \hline
\multirow{3}{*}{Knot-resolver}     & TCP      & \multirow{3}{*}{20 / 40 / 100 / 500 / 1000 / 2000 / 4000 / 8000} \\ \cline{2-2}
                                   & DoT      &                                                                  \\ \cline{2-2}
                                   & DoH      &                                                                  \\ \hline
\multirow{3}{*}{Dnsdist}           & TCP      & \multirow{3}{*}{20 / 40 / 100 / 500 / 1000 / 2000 / 4000 / 8000} \\ \cline{2-2}
                                   & DoT      &                                                                  \\ \cline{2-2}
                                   & DoH      &                                                                  \\ \hline
\multirow{3}{*}{Adguard-dnsproxy}  & TCP      & \multirow{3}{*}{20 / 40 / 100 / 500 / 1000 / 2000 / 4000 / 8000} \\ \cline{2-2}
                                   & DoT      &                                                                  \\ \cline{2-2}
                                   & DoH      &                                                                  \\ \hline
\end{tabular}
\end{adjustbox}
\caption{Parameters used when measuring the cost of handling queries}
\label{tbl:conn_queries_parameters}
\end{table}

\begin{figure*}[!ht]
        \centering
        \includesvg[inkscapelatex=false, width=\textwidth]{queries_no_open}
        \caption{Queries per second handled by \replaceforme{both}{the} servers when connections last all experiment}
        \label{fig:conn_queries}
\end{figure*}
\subsection{Cost of handling queries}
\label{subsection:use}

While the use of these new connected protocols seems to cause no issue regarding memory consumption, it can induce a CPU overhead due to the additional steps required when handling messages. These can be broken down into two parts~: first, the connection establishment, and then, the handling of individual messages (see section \ref{sec:technical_background}).
%
The experiment described here aimed to estimate the cost of handling individual queries. \ITUpar

In order to measure the additional cost per request, we must take into consideration the number of simultaneously opened connections over which requests are sent. To do this, we sent a fixed amount of traffic over a set number of already opened connections. We repeat that experiment multiple time for each protocol / server combination, with a variable number of connections for each experiment. The various experimental parameters chosen are presented in table \ref{tbl:conn_queries_parameters}.
%
The total fixed amount of queries sent, as well as the minimum number of connections was chosen to ensure that the CPU utilisation of the server process and the frequency of the core it ran on, were as high as possible.
%
%% \replacefond{We ran into an issue when using Flamethrower, meaning we could not exceed a certain number of queries per second with DoH, so the number of queries per second sent is not the same for DoH as it is for DoT and TCP. However, all tested configurations allowed us to reach 100\% CPU use by the server process, which means that this issue does not affect the validity of our results. \ITUpar}{We ran into an issue when using Flamethrower, meaning we could not send more than 140000 queries per second with DoH, so the number of queries per second sent is not the same for DoH as it is for DoT and TCP. However, all tested configurations lead to the server being scheduled on the single CPU that was alloted to it 100\% percent of the time (which is the same as all the other configurations), so we do not believe that this gap in experimental parameters had an impact on the measured performances.}

Each point on Figure \ref{fig:conn_queries} represents the average number of queries per second that were successfully answered by the tested server, with bars presenting the minimum and maximum value reached, for a specific protocol and a specific number of connections. We also represented the max traffic handled with UDP for comparison purposes.
%
For every connected protocol there is a performance drop when compared to UDP\replacefond{, due to the management of the state associated with the connections}{. This is partly because, at the kernel level, handling a message sent over a TCP connection involves extra steps when compared to UDP, such as checking the consistency of acknowledgment numbers or calculating the congestion window size. Furthermore,}
%
we observe that, for Dnsdist, there is a noticeable drop in performances between TCP \replaceforme{any}{and} DoT, while for Knot-resolver and Adguard-dnsproxy, their performances are very similar.
%
We explain these differences by the fact that Knot-resolver or Adguard-dnsproxy are less efficient at handling DNS queries, meaning that the cost added by symmetric encryption is absorbed by the cost of handling DNS queries.
%
As Dnsdist is more efficient, the per-message cost added by symmetric encryption is more noticeable.
%
When comparing DoH to other protocols, we notice, for all three servers, a huge drop in performances (by a factor of two), that we explain by the added cost of handling the HTTP/2 protocol layers\replacefond{.}{, which includes sending and receiving the control messages, allocating the data structures for new streams, and using the \gls{HPACK} algorithm to decode message headers.}
%
For every protocol, we observe that performances tend to drop when the number of connection increases (up to a 40\% decrease when considering Dnsdist over DoH), except for Dnsdist over TCP.

\begin{table}[!ht]
\begin{adjustbox}{max width=\columnwidth}
\begin{tabular}{|c|c|c|c|}
\hline
Software                          & Protocol & \multicolumn{1}{l|}{Number of connections} & Connection duration                                \\ \hline
\multirow{3}{*}{Knot-resolver}    & TCP      & \multirow{9}{*}{1000}                      & \multirow{3}{*}{1 query per connection / 1s / 30s} \\ \cline{2-2}
                                  & DoT      &                                            &                                                    \\ \cline{2-2}
                                  & DoH      &                                            &                                                    \\ \cline{1-2} \cline{4-4} 
\multirow{3}{*}{Dnsdist}          & TCP      &                                            & \multirow{3}{*}{1 query per connection / 1s / 30s} \\ \cline{2-2}
                                  & DoT      &                                            &                                                    \\ \cline{2-2}
                                  & DoH      &                                            &                                                    \\ \cline{1-2} \cline{4-4} 
\multirow{3}{*}{Adguard-dnsproxy} & TCP      &                                            & \multirow{3}{*}{1 query per connection / 1s / 30s} \\ \cline{2-2}
                                  & DoT      &                                            &                                                    \\ \cline{2-2}
                                  & DoH      &                                            &                                                    \\ \hline
\end{tabular}
\end{adjustbox}
\caption{Parameters used when measuring the cost of establishing connections}
\label{tbl:open_close_parameters}
\end{table}

\begin{figure*}[!ht]
        \centering
        \includesvg[inkscapelatex=false, width=\textwidth]{open_close}
        \caption{Queries per second (qps) handled per server and protocol according to number of connection establishments per second}
        \label{fig:open_close}
\end{figure*}

\subsection{Overhead of establishing connections}
\label{subsection:establishment}

In the following experiment, we measure the cost of opening and closing connections for each protocol.
%
We use the same experimental parameters as the one used in the previous experiment (figure \ref{fig:conn_queries}) to plot the \replaceforme{point}{points} corresponding to 1000 connections, but this time taking into account connection establishment and tear down. Thus, by comparing this experiment and the previous one, we can infer the overhead of connection establishment (TCP connection establishment, TLS key exchange).
%
We run two sets of experiments, one in which our clients close and re-establish a connection every 30s, the other in which clients close and re-establish connections every second (to match the long and short-lived connections we observe in \ref{sec:characterization_DNS_client_behaviour}). The parameters used for this experiment are presented in table \ref{fig:open_close}.
%
In order to avoid the case in which the server has to handle a batch of connections establishment every 1 or 30 seconds, then only queries, and then another batch of connection establishment, we \replacefond{launch all of our clients with a set delay between every launch, ensuring that every connection establishment is offset in such a way so that the server has to handle a constant amount of connection establishments per second.}{ensure that clients are started sequentially, with a set delay in between them. This, coupled with the fact that clients are configured to close and re-open connections on a strict timer, means that, in each experiment, the server has to handle a fixed amount of connection establishments every second :}
%(see figure \ref{fig:msg_seqconn_openclose} for a visual representation of the sequence of connection establishments, messages and tear-downs for this experiment)
%
\replacefond{Therefore,}{} in the first experiment, the server has to handle 33 connection establishments every second, while in the second experiment, it has to handle 1000 connection establishments every second.
%
\replacefond{}{In addition to being set by design, these rates were confirmed to have been respected through an analysis of the network trace generated by each experiment.}
%
We run an additional batch of experiments to reproduce the very short-lived connections also observed in section \ref{sec:characterization_DNS_client_behaviour}~: during this experiment, the clients are configured to establish a connection, send a single query over it, wait for its answer, close the connection and immediately start over.

The results of the \replaceforme{experiment}{experiments} are presented in Figure~\ref{fig:open_close}, \replaceforme{with}{in the form of} the number of queries per second handled by \replaceforme{the}{a} server for each combination of protocol and connection duration. We also plotted the maximum queries per second we reached in the previous experiment (figure \ref{fig:conn_queries}) as a baseline for comparison.
%
\replaceforme{In general for}{For} all three servers, we observe the same performance variations between protocols, but with different orders of magnitude~:
%
when few connection establishments occur (33 connection establishments per second), we observe a variable decrease in performances (of up to 20\%) for TCP and DoT, relative to \replaceforme{when}{the experiment in which} no \replaceforme{connections establishment}{connection establishments} occur. However, we do not observe this performance loss for DoH, as the CPU cost of query handling \replaceforme{still is}{is still} the bottleneck.
%
When the frequency of connection establishments increases (connections last for 1 second), the performances of TCP decreases less \replaceforme{thant}{than} that of both encrypted protocols\replaceforme{}{, as they both} see a decrease in performances due to the added cost of the TLS key exchange.
%
When connections are used for one query only, the cost of establishing TCP connections induces a collapse of performances for all protocols.

%% \begin{figure}[!ht]
%%      \centering
%%      \includesvg[inkscapelatex=false]{cpu_times_percent_total_halfwidth}
%%      \caption{Proportion of the scheduled time of the resolver process spent in each mode}
%%      \label{fig:cpu_use_percent}
%% \end{figure}

\begin{figure*}[!ht]
	\centering
        \includesvg[inkscapelatex=false, width=\textwidth]{cpu_times}
	\caption{\replacefond{Proportion of the scheduled time of}{Percentage of its scheduled time} the resolver process spent in each mode}
	\label{fig:cpu_use_percent}
\end{figure*}

\subsection{CPU usage of resolver process}

We also measured the time spent by the processes of Knot-resolver, Dnsdist and Adguard-dnsproxy in kernel or user mode. Figure \ref{fig:cpu_use_percent}, presents, for these severs, the percentage of their total scheduled time they spent in user or kernel mode, depending on the amount of connections per second they had to handle. We base our analysis on the fact that, for these software, TCP and UDP is handled in the kernel, while TLS and HTTP/2 are handled in userland. \ITUpar

UDP is a very simple protocol implemented \replaceforme{entierly}{entirely} in the kernel. As there is no connection or encryption, all of the user time is spent on the DNS resolution service, and not on the handling of higher protocol layers, such as TLS or HTTP/2. Therefore, comparing how the servers handle it shows us how they handle the base DNS protocol.
%
We can infer, from the fact that Knot-resolver and adguard spend a higher fraction of their time in user mode when compared to Dnsdist, for a lower amount of queries handled, that they spend more time handling base DNS messages than Dnsdist, which is consistent with the fact that they are less efficient than Dnsdist at handling base \replaceforme{dns}{DNS} queries.

Studying the behaviour of these servers on more complex protocols, we observe that handling DoT or DoH \replaceforme{result}{results} in more time spent in userland. This phenomenon is less marked for Knot-resolver when it is handling few DoH connection establishments (No open or 33cx/s on figure \ref{fig:cpu_use_percent}(a)), as it makes more per-query syscalls then Dnsdist or adguard in order to handle the HTTP/2 protocol.

For all three servers however, the more TLS connection establishment occur (which is the case when handling DoT or DoH at 1000cx/s or 1q/cx), the more time is spent in userland, as this is where the (costly) TLS handshake is handled.


\begin{table*}[t]
\centering
\resizebox{\textwidth}{!}{
\begin{tabular}{|c|ccc|
>{\columncolor[HTML]{FFFFFF}}c
>{\columncolor[HTML]{FFFFFF}}c
>{\columncolor[HTML]{FFFFFF}}c |ccc|}
\hline
                            & \multicolumn{3}{c|}{Knot-resolver}                                                                                                & \multicolumn{3}{c|}{\cellcolor[HTML]{FFFFFF}Dnsdist}                                                                                                                              & \multicolumn{3}{c|}{Adguard}                                                                                                       \\ \hline
Protocol                     & \multicolumn{1}{c|}{1 q/cx}                      & \multicolumn{1}{c|}{1000cx/s}                    & 33cx/s                      & \multicolumn{1}{c|}{\cellcolor[HTML]{FFFFFF}1 q/cx}                      & \multicolumn{1}{c|}{\cellcolor[HTML]{FFFFFF}1000cx/s}                    & 33cx/s                      & \multicolumn{1}{c|}{1 q/cx}                       & \multicolumn{1}{c|}{1000cx/s}                    & 33cx/s                      \\ \hline
UDP                          & \multicolumn{3}{c|}{0.12}                                                                                                         & \multicolumn{3}{c|}{\cellcolor[HTML]{FFFFFF}0.05}                                                                                                                                 & \multicolumn{3}{c|}{0.19}                                                                                                          \\ \hline
{\color[HTML]{3274A1} TCP} & \multicolumn{1}{c|}{{\color[HTML]{3274A1} 3.1}}  & \multicolumn{1}{c|}{{\color[HTML]{3274A1} 0.34}} & {\color[HTML]{3274A1} 0.33} & \multicolumn{1}{c|}{\cellcolor[HTML]{FFFFFF}{\color[HTML]{3274A1} 3.08}} & \multicolumn{1}{c|}{\cellcolor[HTML]{FFFFFF}{\color[HTML]{3274A1} 0.08}} & {\color[HTML]{3274A1} 0.08} & \multicolumn{1}{c|}{{\color[HTML]{3274A1} 1.87}}  & \multicolumn{1}{c|}{{\color[HTML]{3274A1} 0.29}} & {\color[HTML]{3274A1} 0.2}  \\ \hline
{\color[HTML]{3A923A} DoT}   & \multicolumn{1}{c|}{{\color[HTML]{3A923A} 8.97}} & \multicolumn{1}{c|}{{\color[HTML]{3A923A} 0.52}} & {\color[HTML]{3A923A} 0.28} & \multicolumn{1}{c|}{\cellcolor[HTML]{FFFFFF}{\color[HTML]{3A923A} 9.46}} & \multicolumn{1}{c|}{\cellcolor[HTML]{FFFFFF}{\color[HTML]{3A923A} 0.3}}  & {\color[HTML]{3A923A} 0.12} & \multicolumn{1}{c|}{{\color[HTML]{3A923A} 17.48}} & \multicolumn{1}{c|}{{\color[HTML]{3A923A} 2.42}} & {\color[HTML]{3A923A} 0.24} \\ \hline
{\color[HTML]{E1812C} DoH}   & \multicolumn{1}{c|}{{\color[HTML]{E1812C} 9.67}} & \multicolumn{1}{c|}{{\color[HTML]{E1812C} 0.77}} & {\color[HTML]{E1812C} 0.46} & \multicolumn{1}{c|}{\cellcolor[HTML]{FFFFFF}{\color[HTML]{E1812C} 9.85}} & \multicolumn{1}{c|}{\cellcolor[HTML]{FFFFFF}{\color[HTML]{E1812C} 0.73}} & {\color[HTML]{E1812C} 0.19} & \multicolumn{1}{c|}{{\color[HTML]{E1812C} 19}}    & \multicolumn{1}{c|}{{\color[HTML]{E1812C} 3.27}} & {\color[HTML]{E1812C} 0.97} \\ \hline
\end{tabular}
}
\caption{Estimation, \textbf{in kWh} of the energy consumed by running 10k QPS for one day using the measured profiles}
\label{tbl:watts_24h_10kqps}
\end{table*}

\subsection{Resolver power consumption}

\replacefond{We measured the energy consumption of the server, first at idle, then during the experiments (when one core is fully loaded), and the difference between the two gives us the lower bound of the energy consumed by the various protocols. In order to compare them we compute the cost for one request and use this cost to obtain the energy consumed, in kWh, of handling 10k QPS for a day (results in table \ref{tbl:watts_24h_10kqps}).}{We measured the energy consumption of the server, first at idle, then during the experiments (when one core is fully loaded), and the delta between the two gives us the lower bound of the energy consumed by the various protocols. This delta is then used to compute the cost, in KWh, of one request and we use this cost to obtain the energy consumed, in kWh, of handling 10k QPS for a day (results in table \ref{tbl:watts_24h_10kqps}). As this cost does not include the idle consumption, if the cost per-query is really low (as is the case with DNS over UDP), our projection results in low estimated energetical costs. In the opposite case, when the cost per query is very high, this can result in a very high estimated energetical consumption, as handling 10K QPS in this case, would necessitate running several cores at 100\%, or even running additional machines.}
%
If connection use is fair (30s) the use of secured protocols is worth considering, even though it increases energy consumption by a factor of two for DoT and four for DoH, but when we consider a higher load, in which connection openings occur more frequently the use of such protocols become more costly (up to 15 times for DoH at 1s), or un-sustainable in the case of aggressive connection opening (0.05 kWh versus 9.85 kWh).

Despite its worse overall power consumption, Adguard-dnsproxy distinguishes itself on TCP connection establishment, as it shows a 30\% performance gain over the other two.

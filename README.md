# How to build

The present document was built on Debian-testing, with a trivial Makefile
that runs latexmk.

The debian packages necessary for compilation are the following :

`latexmk make texlive-xetex inkscape texlive-latex-recommended texlive-bibtex-extra texlive-science biber`

To avoid incompatibilities, we provide a Docker image that successfully
compiles the document. The script `build_document.sh` builds the image and runs
it. The first arg is passed to this script is passed to the docker image
(so you can run the "clean" target of the makefile by running
`build_document.sh clean`.

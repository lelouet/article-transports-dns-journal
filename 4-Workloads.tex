As the newer DNS transports are \replaceforme{connection based}{connection-based}, new questions arise:
%
while the protocol defines how to query the service, it doesn't specify how the underlying connections should be managed by both the client and resolver.
%
The sequence of connections opening and requests sent is mostly controlled by the client\replacefond{ but}{. But} to focus only on the client's behaviour is not enough, as the server has the choice to accept, reject, close or keep said connections opened. \ITUpar

The objective of this experiment is to characterise the shape, in terms of number connection establishments, closures and messages sent over individual connections, of the traffic between already existing clients and publicly available resolvers. We specifically choose existing clients and public resolvers who are widely used and whose configuration and behaviour we do not control, as we want to determine how \replaceforme{the}{} the implementations currently deployed use the protocol, so we can generate similar traffic when measuring server-side performances. \ITUpar

In section \ref{workload_subsection:experimental_setup} we describe the experimental setup used for the measurements while section \ref{workload_subsection:results} contains an analysis of the different behaviours observed from the clients and the resolvers.

\subsection{Experimental setup}
\label{workload_subsection:experimental_setup}

As DoH gained more traction than DoT and is therefore available in more software and on more public resolvers, the only traffic generated in this experiment is DoH traffic.
%
To characterise the shape of the traffic, we make a DoH-enabled client send queries at various rates (one every 50 ms, 1000 ms and 60~000 ms) for 30 minutes, to a DoH-enabled public resolver.
%
\replacefond{We run the client in a container, as the virtual network interfaces created by the container engine give us a way to isolate its network traffic for capture using Tshark \cite{tshark}.}{We run the client in a docker container for two reasons : the isolation provided by the network name spaces give us a way to isolate its network traffic for capture using Tshark \cite{tshark}, and using a container allows for easier reproducibility of our experiment. We enforce no resource restrictions on the container we use, therefore the only overhead is the additional network latency and CPU use due to the more complex network path induced by the use of network name spaces.}
%
We then further filter the network traffic based on the target's resolver IP, the port used by DoH (443), and transport protocol (TCP), leaving us with a trace containing only the DoH traffic emitted by the client.
%
We then extract the following metrics from the network trace~: the number of TCP connections established between client and resolver, their duration, the origin (client or the server) and method (TCP \gls{FIN} or \gls{RST}, HTTP/2 \gls{GOAWAY}) of its closure.
%
Another metric we consider is the number of queries on each connection. However, as traffic is encrypted, we make our client dump the TLS secrets to a file, in the \acrfull{nss} Key Log Format, so that \replaceforme{thank}{Tshark} can decipher the traffic, allowing us to count the exact number of queries and responses exchanged in every TCP connection. \ITUpar

Part of the reason for DoH's popularity is its integration in popular web browsers. For this reason, we elected to observe the behaviour of Chromium \cite{chromium}, as it is the basis for other popular browsers \cite{browser-stats}, such as Chrome or Edge. We also chose to observe Firefox's \cite{firefox} behaviour, as it was among the first browsers to implement a DoH stub resolver. \ITUpar

Even though web browsers are likely to be one of the biggest source of DNS traffic, there are other software on an end-user's machine that can generate DNS traffic as well, and, in the majority of cases, said traffic is unsecured, as it relies on the stub resolver provided by the host OS.
%
A new category of software, called proxies, has emerged to resolve this issue.
%
They run on the user's device, listening on a local port, and are configured as the system's resolver, meaning that they capture all DNS queries emitted by software on the system,
%
and transmit them to a configured resolver over a secured channel. This means that, by installing and configuring it, all software transparently benefits from a secured DNS channel to a trusted resolver, that is shared among all running applications, which saves both client and server resources when compared to a model in which each client individually implements secured DNS transports. \ITUpar

As the goal of this experiment is to measure how both parties use the underlying TCP connections, we need to measure how current DNS over HTTPS deployments handle traffic. This is why we chose to observe the behaviour of three widely used, DoH-enabled public resolvers~: Quad9, Google and Cloudflare.
%
Since we have 3 different clients, delays and resolver, this brings the total amount of experiments we run to test every combination to 27. \ITUpar

We gather the list of domain names to resolve from a public list of domains names \cite{samplequeryfile}, filtered to keep the ones that still have an A record corresponding to a server accepting HTTP traffic.
%
In order to generate the appropriate traffic using the browsers, we configure them to use the selected DoH resolver, and we loaded a JavaScript script making HTTP \gls{HEAD} requests to the domain names in the file. We choose HTTP HEAD request, as they require the browser to resolve the domain name to contact the server, but cause very little data to be returned.
%
To generate traffic using DNScrypt-proxy, we use a C program making DNS resolutions for the domain in the list at the same rates as the ones configured for the browser, using the system's configured resolver, which, in this case is DNSCrypt-proxy.

\replacefond{}{Both these scripts, in addition to the configuration files for the browsers, DNSCrypt-proxy, as well as the scripts used to analyse the traffic, are available in a public git repository \cite{doh-client-analysis-scripts}}.

\subsection{Results}
\label{workload_subsection:results}

\begin{figure}[!ht]
\begin{subfigure}{0.9\columnwidth}
	\centering
	\includegraphics[width=\columnwidth]{./images/firefox.png}
	\caption{Client~: Firefox}
	\label{fig:both_conn_profile_firefox}
\end{subfigure}

\begin{subfigure}{0.9\columnwidth}
	\centering
	\includegraphics[width=\columnwidth]{./images/Chromium.png}
	\caption{Client~: Chromium}
	\label{fig:both_conn_profile_Chromium}
\end{subfigure}

\begin{subfigure}{0.9\columnwidth}
	\centering
	\includegraphics[width=\columnwidth]{./images/dnscrypt.png}
	\caption{Client~: DNSCrypt-proxy}
	\label{fig:both_conn_profile_dnscrypt}
\end{subfigure}

\caption{Connection use by clients (Firefox, Chromium and DNSCrypt-proxy) for a set of resolvers (Cloudflare, Google and Quad9) and query delays (50ms, 1000ms 60~000ms)}
\label{fig:both_conn_profile}
\end{figure}

Figures~\ref{fig:both_conn_profile_firefox}, \ref{fig:both_conn_profile_Chromium}, \ref{fig:both_conn_profile_dnscrypt} presents, for each combination of software, inter-query delay and public resolver, the number and length of connections to the resolver established by the client. For example, on figure \ref{fig:both_conn_profile_dnscrypt}, the top-left figure presents the number and length of TCP connections that DNSCrypt established towards the Cloudflare resolver.
%
\replacefond{The horizontal axis of each sub-figure}{Each sub-figure can be seen as a Gantt diagram : the x-axis} represents the time in the experiment, and every connection \replacefond{is presented as}{is represented on a single line} as a coloured rectangle, its leftmost and rightmost edges marking its start and end date respectively. Connections shorter than a second are represented by a cross.
%
For example, by observing the top-right graph of figure \ref{fig:both_conn_profile_dnscrypt}, we can see that, when the inter-query delay is 50ms, DNScrypt-proxy established only one connection to the quad9 resolver.
%
Conversely, by observing the top-right graph of figure \ref{fig:both_conn_profile_firefox}, we observe that, when faced with the same inter-query delay of 50ms, Firefox established a lot of short-lived (less than 1s) connections to the quad9 resolver.

\paragraph{DNScrypt-proxy}

DNScrypt-proxy generates the least aggressive load towards the server.
%
Indeed, its main behaviour is to open and keep opened a single TCP connection that it will use to perform all requests, regardless of intensity of the traffic generated\replaceforme{, we}{. We} can infer this by observing the middle row two top-rightmost graphs on figure \ref{fig:both_conn_profile_dnscrypt}.
%
In addition, an internal timer is set to trigger the close of the TCP connection 5 seconds after the last message was sent over it, freeing both client and server resources. We infer this by observing the bottom row on figure \ref{fig:both_conn_profile_Chromium}, or the network trace, in which we see that the TCP connection closure is always initiated by DNSCrypt-proxy, through a TLS alert message with the "Close Notify" description.

\paragraph{Web browsers}

The browsers have a more aggressive usage of DNS resources: at the beginning of the sessions, they try to maximise the probability of having a successful connection to the DNS server by opening several connections in parallel to the same server, likely to speed up the early resolutions that browsers usually do, (figure \ref{fig:both_conn_profile_firefox} and \ref{fig:both_conn_profile_Chromium}), leading to an increase in server resources usage.
%
The following use of these opened connections depends on the intensity of the traffic.
%
When the traffic has a low intensity, with a request frequency lower than 1 \acrfull{qps}, see the bottom two rows of figures \ref{fig:both_conn_profile_firefox} and \ref{fig:both_conn_profile_Chromium}), a single connection is mainly used to handle the traffic, the remaining connections eventually being closed.
%
We sometimes observe connection closures, forcing a re-opening (1000ms delay row on Figures \ref{fig:both_conn_profile_Chromium} and \ref{fig:both_conn_profile_Chromium}), or multiple connections at the same time (1000ms and 60~000ms delay rows on figure \ref{fig:both_conn_profile_Chromium}), but these events are not numerous enough during the lifetime of an experiment to be significant.
%
Under a DNS traffic with a high intensity (above 1 query per second) the connection pattern of the web browsers changes drastically. Not only does the browser fails to generate the traffic we ask for, we also observe connections being opened and closed in sequence, (see top row on figures figures \ref{fig:both_conn_profile_firefox} and \ref{fig:both_conn_profile_Chromium}). Every connection shutdown originates from the client, either through an HTTP/2 GOAWAY message for Firefox, or directly through a TCP FIN message for Chromium. Each of these connections is used to carry few to no DNS message.
%
From a server perspective, such behaviour represents the worst case, as, with each connection opening being costly, this leads to huge resource consumption. 

\paragraph{Resolvers}

Clients are not the sole responsible for the connection patterns. The resolvers have the choice to accept or deny the connections and the traffic issued from the clients. We have observed that Google has the most permissive resolver configuration of those we tested, as we didn't observe any limitation in terms of number of connections, their duration and the number of QPS per connection.
%
Quad9 closes unused connection through an HTTP/2 GOAWAY message after around 30 seconds of inactivity (figure\ref{fig:both_conn_profile_firefox}, bottom-right).
%
Cloudflare does not impose any restriction on the connection duration, but limits the maximum number of requests per connection to 10~000 (figure \ref{fig:both_conn_profile_Chromium}, top-left). When this limit is reached, the server notifies the client through an HTTP/2 GOAWAY message paired with a TLS alert message with the "Close Notify" description. \ITUpar

The intended behaviour of clients and resolvers seems to be to keep one TCP/TLS connections alive while they are used, as re-opening a connection leads to \replaceforme{cost}{an increased cost} in CPU resources (as the TLS handshake is relatively costly), or in latency (as each connection establishment requires multiple round-trips).

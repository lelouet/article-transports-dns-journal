.PHONY: clean all article.pdf coverletter.pdf coverletter-reponse.pdf

all: article.pdf coverletter.pdf coverletter-reponse.pdf


article.pdf:
	latexmk -pdf -xelatex -shell-escape -use-make article.tex

coverletter.pdf:
	latexmk -pdf -xelatex -shell-escape -use-make coverletter.tex

coverletter-reponse.pdf:
	latexmk -pdf -xelatex -shell-escape -use-make coverletter-reponse.tex

clean:
	latexmk -C article.tex
	latexmk -C coverletter.tex
	latexmk -C coverletter-reponse.tex
	rm -f *.run.xml *.bbl

fullclean: clean
	rm -rf svg-inkscape

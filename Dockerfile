FROM debian:testing

ENV DEBIAN_FRONTEND=noninteractive
ARG USER_ID
ARG GROUP_ID

RUN apt update && apt install -y latexmk make texlive-xetex inkscape texlive-latex-recommended texlive-bibtex-extra texlive-science biber && rm -rf /var/lib/apt/lists/*

WORKDIR /src

ENTRYPOINT [ "make" ]

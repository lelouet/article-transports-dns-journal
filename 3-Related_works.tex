We classify the works related to DNS security in three categories:
%
works that focus on describing and proposing mitigation for different security flaws,
%
works that aim to compare the client-side cost of secured DNS protocols,
%
and finally, works that focus on evaluating their adoption.

\paragraph{Security issues and guarantees}

When studying the security of an information system, three properties are to be considered~: confidentiality, integrity and availability. \ITUpar

\textbf{Confidentiality}~: UDP and TCP did not offer any kind of confidentiality \replaceforme{for messages}{to the messages they carry}, meaning that any malicious actor could use captured DNS message to breach a user's privacy \cite{rfc7626}.
%
DoT and DoH circumvent this flaw by using the TLS protocol to carry their messages, therefore guaranteeing confidentiality. However, several studies have shown that some characteristics of DNS traffic can be exploited to, in some cases, de-anonymize encrypted DNS traffic.
%
In \cite{https://doi.org/10.48550/arxiv.1906.09682}, Siby et al. show that, despite its use of encryption, it is still possible to determine the content of
a DoH flow containing un-padded queries and responses, by using traffic analysis techniques.
%
In \cite{DBLP:conf/uss/BushartR20}, Bushart and Roshow show that even state-of-the-art padding strategies are weak against some traffic analysis attacks.
%
However, it is worth noting that the \replaceforme{models}{machine-learning models} used in these attacks can only de-anonymize DNS flows they were previously trained on (usually popular websites), and that techniques such as arbitrarily delaying queries and responses, or the use of proxy networks such as \acrfull{tor} can be powerful mitigation against these attacks. Furthermore, these attacks require both a constant update of the model used to target websites in order to cope with their modification, and the knowledge of the source of the traffic, as clients have different behaviours regarding inter-query timings and message size. \ITUpar

\textbf{Integrity}~: The DNS protocol did not initially offer mechanisms guaranteeing the integrity of data, meaning that an adversary could edit a DNS response, thus redirecting a client towards fraudulent services \cite{8550085}. DNSSEC was later standardised, and guarantees the integrity of data exchanged between the resolver and name servers. On the other hand, the data exchanges between client and resolver still use the legacy protocol, leaving them vulnerable to the aforementioned attacks.
%
As TLS guarantees the integrity of the messages it transports, using DoT or DoH in combination with a trusted resolver that validates the integrity of records by using DNSSEC, can protect against this category of attacks. \ITUpar

\textbf{Availability}~: The two aforementioned properties are necessary but not sufficient to fully protect a client. DNS is one of the most commonly filtered protocols (by governments or \acrfull{isp} \cite{lowe2007great}). DoT, which uses port 853 by default can be easily blocked by port-based filters, while DoH is not, as it relies on a widely used protocol. It is still vulnerable to fingerprinting techniques, able to detect whether or not an encrypted flow contains DoH queries and response, like Vekshin et al. prototyped in \cite{10.1145/3407023.3409192}. However, as we said earlier, these techniques \replaceforme{requires}{require} models trained on a variety of clients, resolver and traffic shape that require constant updating, so it is unrealistic to expect them to be used globally.
%
Despite the remaining security limitations, the benefits provided by DoT and DoH complete the efforts first undertaken with the introduction of DNSSEC.

\paragraph{Client-side performance}

Various studies focus on the client-side cost of DoT or DoH:
%
Hounsel et al.~\cite{comparing_the_effects_of_DNS_DoT_DoH_on_web_performance}, \cite{10.1145/3340301.3341129} compare the page load times using different combinations of DNS transports, network types and public resolvers.
%
Boettger et al.~\cite{an_empirical_study_of_the_cost_of_DoH} also compare the resolution times and protocol overhead of different secure DNS transports when using persistent or non-persistent connections.
%
These studies find that connection reuse is beneficial for the client, and that secure DNS adds no noticeable cost to clients, except on some cellular networks.

\replacefond{}{Chhabra et al. \cite{10.1145/3487552.3487849} also study the impact of switching to DoH, leveraging several vantage points, which allows them to correlate the speedup or slowdown measured when switching from \acrfull{doudp} to DoH with the level of investment in internet infrastructure by a country.}

\replacefond{}{Finally, in \cite{10.1145/3517745.3561445}, Kosek et al. compare the performances (in terms of latency) of DoH with those of DoUDP, but also with those of \acrfull{doq}. They found that the use of DoQ lead to faster page load times than with DoH, due the faster connection establishment. However, said improvement over DoH lessens as the complexity of the loaded page (and therefore the number of resolutions needed to load it) increases, since the latency gained by a faster handshake is amortized by the connection reuse, which in turns allows both DoH and DoQ (connection-based protocols) to catch up with DoUDP (as discussed by Boettger et al. \cite{an_empirical_study_of_the_cost_of_DoH}).}

\paragraph{Protocol Adoption}

In \cite{garcia2021large} Garcìa et al. analyse both the number of available DNS-over-encryption resolvers, as well as the use of DNS-over-encryption by various users. While the amount of DoH traffic had stayed stationary, representing about 1\% of the current DNS traffic, the number of available DoH server is steadily growing\replaceforme{, leaving the question of energetic sustainability, if their use is generalised, opened.}{. Such a growth raises the question of the energetical sustainability of the generalized use of DNS over HTTPS.} 

\replacefond{}{While the works discussed here offer valuable insights on the more recent DNS transport protocols, ours is the only work that focuses on the impact on resolver resource consumption of implementing such protocols : the existing literature focuses on client-side or network-side metrics such as latency or packets number or size, while our work focuses on the memory or CPU consumption of implementing such protocols, on the server-side.}
